package com.company.goods;

import com.company.departments.BaseDepartment;

public class BaseGoods {
    private BaseDepartment department;

    public BaseGoods(BaseDepartment department) {
        this.department = department;
    }
}
