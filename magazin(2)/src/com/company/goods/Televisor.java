package com.company.goods;

import com.company.departments.BaseDepartment;

public class Televisor extends ElectronicDevice{

    private String company;
    private String model;

    public Televisor(BaseDepartment department, String name, boolean hasGuarantee, String price, String company, String model) {
        super(department, name, hasGuarantee, price);
        this.company = company;
        this.model = model;
    }

    public void on(){

    }
    public void off(){

    }
    public void selectChannel(){

    }

}
