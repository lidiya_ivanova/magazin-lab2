package com.company.goods;

import com.company.departments.BaseDepartment;

public class GameConsole extends ElectronicDevice {

    private String ram;
    private String company;

    public GameConsole(BaseDepartment department, String name, boolean hasGuarantee, String price, String ram, String company) {
        super(department, name, hasGuarantee, price);
        this.ram = ram;
        this.company = company;
    }

    public void on(){

    }
    public void loadGame(){

    }

}
