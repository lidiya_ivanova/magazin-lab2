package com.company.goods;

import com.company.departments.BaseDepartment;

public class HardDrive extends ElectronicDevice{

    private String volume;
    private String company;

    public HardDrive(BaseDepartment department, String name, boolean hasGuarantee, String price, String volume, String company) {
        super(department, name, hasGuarantee, price);
        this.volume = volume;
        this.company = company;
    }

    public void format() {

    }
    public void copy(){

    }

    public void delete(){

    }
}
