package com.company.service;

import com.company.bank.BaseBank;
import com.company.departments.BaseDepartment;

public class Banker extends BaseEmployee{
    private BaseBank bank;

    public Banker(String name, boolean free, BaseDepartment department, BaseBank bank) {
        super(name, free, department);
        this.bank = bank;
    }

    public void sendRequest(){

    }

}
