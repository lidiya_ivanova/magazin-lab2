package com.company.service;

import com.company.departments.BaseDepartment;

public class Security extends BaseEmployee{

    public Security(String name, boolean free, BaseDepartment department) {
        super(name, free, department);
    }

    public void openDoor(){

    }
    public void closeDoor(){

    }
    public void checkVisitor() {

    }

}
