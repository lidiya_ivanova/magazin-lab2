package com.company.departments;

import com.company.goods.BaseGoods;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public class HomeDepartment extends BaseDepartment{
    public HomeDepartment(String name, ArrayList<BaseGoods> goods, ArrayList<BaseEmployee> employees) {
        super(name, goods, employees);
    }
}
